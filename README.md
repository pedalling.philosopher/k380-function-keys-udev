This is a simple udev rule that enables the function keys
on the k380 Logitech bluetooth keyboard (046D:B342).

Use as: /etc/udev/rules.d/70-k380-function-keys.rules

I was tired of re-inventing this udev rule every time I
move this keyboard. So in gitlab it goes.
